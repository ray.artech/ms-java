## MICROSERVICIOS
Es una arquitectura de desarrollo que consiste en construir una applicación como un conjunto de servicios independientes entre sí (estos se ejecutan en su propio proceso), que se comunicaráán por medio de mecanismos ligeros (por lo general usan API REST)

#### Ventajas de usar MICROSERVICIOS
* Escalibilidad (crecimiento es a demanda)
* Modularidad (funciones acotadas, simples y eficientes)
* Hetereogenidad (cada ms puede desarrollarse con su propio stack tecnologico)
* Desacoplamiento (despliegues independientes, maneja su propia BD)
* Velocidad de despliegue (aplicando IC y DC por medio de contenedores)


#### Configuraciones a realizar
* Ambientes de despliegue
* Monitorización, donde se van a guardar los logs, y como los controlamos
* Un registro de ms (service registry)
* Base de datos
* Integración continua
* Contenerización
---


## QUE VAMOS A USAR
* Lenguage: JAVA
* Framework: Springboot

### Caracteristicas del Java
Tiene un recolectador de Basura
* (libera memoria) si deja de referenciarse un objeto, se elimina
Multiplataforma (abstrae del so) por que utiliza JVM (maquina virtual)
* Compilador: .java -> compilador -> .class (bytecode: lenguage maquina, compacto)
* Interprete: .class -> maquina virtual -> ejecutable
Seguridad
    * Soporta sandboxing (aisla la maquina virtual (bytecode))    
    * OOP 
    * Polimorfismo
    * Herencia
    * Encapsulamiento
Multi-thread
    * un app puede lanzar varios hijos de ejecución 


